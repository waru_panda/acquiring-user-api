import json
import logging
import random
from datetime import datetime
from decimal import Decimal

import requests
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render

from fast_api_payment.utils import get_unix_timestamp

logging.basicConfig(level=logging.INFO)

ITEMS = [{"name": "Консультация",
          "sno": "osn",
          "payment_object": "service",
          "payment_method": "full_prepayment",
          "price": str(Decimal("1234")),
          "vat": "none",
          "quantity": str(Decimal("1"))}]


def payout(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))
    amount = random.randint(0, 100)

    items = {"amount": amount,
             "description": "Заказ №57487143",
             "merchant": params.get('merchant'),
             "order_id": order_id,
             "testing": "1",
             'PAN': '2200110130044440',
             'sender_id': '6928944751877',
             }

    r = requests.post('{}/api/v1/payout'.format(params.get('address')), data=json.dumps(items))
    logging.info(r.json())
    return HttpResponse(r.content)


def internal_payment(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))
    amount = random.randint(0, 100)

    items = {"amount": amount,
             "description": "Заказ №57487143",
             "merchant": params.get('merchant'),
             "order_id": order_id,
             "testing": "1",
             'PAN': '2200110130044440',
             'month': '01',
             'year': '20',
             'receipt_contact': 'test@example.com',
             'receipt_items': json.dumps(ITEMS, ensure_ascii=False)
             }

    r = requests.post('{}/api/v1/internal_payment'.format(params.get('address')), data=json.dumps(items))
    resp = r.json()
    logging.info(resp)
    threedsdata = resp.get('transaction')
    data = {
        'MD': threedsdata.get('MD'),
        'PaReq': threedsdata.get('PaReq'),
        'TermUrl': '{}/finish3ds/{}'.format(settings.SITE_URL, world),
    }
    threedsurl = threedsdata.get('acs_url')

    context = dict(
        action=threedsurl,
        method='POST',
        values=data,
    )
    template = 'post_redirect.html'
    return render(request, template, context)


def internal_first_recurrent(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))
    amount = random.randint(0, 100)

    items = {"amount": amount,
             "description": "Заказ №57487143",
             "merchant": params.get('merchant'),
             "order_id": order_id,
             "testing": "1",
             'PAN': '2200110130044440',
             'month': '01',
             'year': '20',
             }

    r = requests.post('{}/api/v1/internal_first_recurrent'.format(params.get('address')), data=json.dumps(items), verify=False)
    resp = r.json()
    logging.info(resp)
    threedsdata = resp.get('transaction')
    data = {
        'MD': threedsdata.get('MD'),
        'PaReq': threedsdata.get('PaReq'),
        'TermUrl': '{}/finish3ds/{}'.format(settings.SITE_URL, world),
    }
    threedsurl = threedsdata.get('acs_url')

    context = dict(
        action=threedsurl,
        method='POST',
        values=data,
    )
    template = 'post_redirect.html'
    return render(request, template, context)


def internal_next_recurrent(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))
    amount = random.randint(0, 100)

    items = {"amount": amount,
             "description": "Заказ №57487143",
             "merchant": params.get('merchant'),
             "order_id": order_id,
             "testing": "1",
             'PAN': '2200110130044440',
             'month': '01',
             'year': '20',
             'first_recurrent_transaction': '5jQkxtbAZ93msoMElQVwNH',
             }

    r = requests.post('{}/api/v1/internal_next_recurrent'.format(params.get('address')), data=json.dumps(items))
    logging.info(r.json())
    return HttpResponse(r.content)


def internal_preauth(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))
    amount = random.randint(0, 100)

    items = {"amount": amount,
             "description": "Заказ №57487143",
             "merchant": params.get('merchant'),
             "order_id": order_id,
             "testing": "1",
             'PAN': '2200110130044440',
             'month': '01',
             'year': '20',
             }

    r = requests.post('{}/api/v1/internal_preauth'.format(params.get('address')), data=json.dumps(items))
    resp = r.json()
    logging.info(resp)
    threedsdata = resp.get('transaction')
    data = {
        'MD': threedsdata.get('MD'),
        'PaReq': threedsdata.get('PaReq'),
        'TermUrl': '{}/finish3ds/{}'.format(settings.SITE_URL, world),
    }
    threedsurl = threedsdata.get('acs_url')

    context = dict(
        action=threedsurl,
        method='POST',
        values=data,
    )
    template = 'post_redirect.html'
    return render(request, template, context)


def internal_confirm(request, world):
    params = settings.WORLD.get(world)

    items = {"transaction": "OsSmlNh1QHWw9OSN18GZ24",
             "amount": '40',
             "merchant": params.get('merchant'),
             }

    r = requests.post('{}/api/v1/internal_preauth_confirm'.format(params.get('address')), data=json.dumps(items))
    logging.info(r.json())

    return HttpResponse(r.content)


def internal_refund(request, world):
    params = settings.WORLD.get(world)

    items = {
        'merchant': params.get('merchant'),
        'amount': '49',
        'transaction': 'RTB1b4TR6hQmHLKKt62pQ4',
        'unix_timestamp': get_unix_timestamp(datetime.now()),
    }
    r = requests.post('{}/api/v1/internal_refund/'.format(params.get('address')), data=items)
    logging.info(r.json())

    return HttpResponse(r.content)


def internal_preauth_refund(request, world):
    params = settings.WORLD.get(world)

    items = {
        'merchant': params.get('merchant'),
        'amount': '84',
        'transaction': 'cAIFqyGIephYWrGdMqWbQ3',
        'unix_timestamp': get_unix_timestamp(datetime.now()),
    }
    r = requests.post('{}/api/v1/internal_preauth_refund/'.format(params.get('address')), data=items)
    logging.info(r.json())

    return HttpResponse(r.content)
