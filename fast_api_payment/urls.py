"""fast_api_payment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt

from fast_api_payment.views import Home, Internal, payment, bill_post_api, bill_put_api, bill_get_api, \
    transactions_get_info, \
    callback, success_response, finish_3ds, transaction_get_info, refund_api, recurrent_payment, capture, \
    extended_manual_bill, cancel_subscription, change_subscription
from fast_api_payment import internal_api_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Home.as_view(), name='home'),
    path('internal', Internal.as_view(), name='internal'),
    re_path('pay/(?P<world>[\w]+)', payment, name='payment'),
    re_path('next_recurrent/(?P<world>[\w]+)', csrf_exempt(recurrent_payment), name='recurrent_payment'),
    re_path('create_bill/(?P<world>[\w]+)', bill_post_api, name='create_bill'),
    re_path('active_bill/(?P<world>[\w]+)', bill_put_api, name='active_bill'),
    re_path('get_bill/(?P<world>[\w]+)', bill_get_api, name='get_bill'),
    re_path('finish3ds/(?P<world>[\w]+)', csrf_exempt(finish_3ds), name='3ds'),
    re_path('get_transactions/(?P<world>[\w]+)', transactions_get_info, name='get_transactions'),
    re_path('get_transaction/(?P<world>[\w]+)', transaction_get_info, name='get_transaction'),
    re_path('refund/(?P<world>[\w]+)', csrf_exempt(refund_api), name='refund'),
    re_path('confirm/(?P<world>[\w]+)', csrf_exempt(capture), name='confirm'),
    re_path('extended_bill/(?P<world>[\w]+)', csrf_exempt(extended_manual_bill), name='extended_manual_bill'),
    re_path('cancel_subscription/(?P<world>[\w]+)', csrf_exempt(cancel_subscription), name='cancel_subscription'),
    re_path('change_subscription/(?P<world>[\w]+)', csrf_exempt(change_subscription), name='change_subscription'),

    re_path('callback', csrf_exempt(callback), name='callback'),
    re_path('success_response', csrf_exempt(success_response), name='success_response'),

    re_path('pan_payment/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_payment), name='pan_payment'),
    re_path('payout/(?P<world>[\w]+)', csrf_exempt(internal_api_views.payout), name='payout'),
    re_path('first_recurrent/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_first_recurrent),
            name='first_recur'),
    re_path('internal_next_recurrent/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_next_recurrent),
            name='internal_next_recur'),
    re_path('inpreauth/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_preauth), name='inpreauth'),
    re_path('inconfirm/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_confirm), name='inconfirm'),
    re_path('inrefund/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_refund), name='inrefund'),
    re_path('preauth/(?P<world>[\w]+)', csrf_exempt(internal_api_views.internal_preauth_refund), name='prrefund'),
]
