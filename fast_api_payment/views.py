import json
import logging
import random
from datetime import datetime
from decimal import Decimal

import requests
from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from fast_api_payment.utils import get_unix_timestamp, get_signature

logger = logging.getLogger('__name__')

ITEMS = [{"name": "Консультация",
          "sno": "osn",
          "payment_object": "service",
          "payment_method": "full_prepayment",
          "price": str(Decimal("10")),
          "vat": "none",
          "quantity": str(Decimal("1"))}]


class Home(TemplateView):
    template_name = 'home.html'


class Internal(TemplateView):
    template_name = 'internal.html'


# Совершить платеж
def payment(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             "merchant": params.get('merchant'),
             "testing": '1',
             "amount": 1234,
             "description": 'Заказ №{}'.format(order_id),
             "order_id": order_id,
             "custom_order_id": "123",
             "client_phone": '+7 (951) 592-68-36',
             "client_email": 'tutturu@example.com',
             "client_name": 'Пяточкина Авдотья Викторовна',
             "sysinfo": '',
             "success_url": '{}/success'.format(params.get('address')),
             "fail_url": '{}/fail'.format(params.get('address')),
             "receipt_contact": 'tutturu@example.com',
             "receipt_items": json.dumps(ITEMS, ensure_ascii=False, sort_keys=True),
             'start_recurrent': '1',

             'autosubscription': '1',
             'autosubscription_period': '2',
             'autosubscription_interval': 'day',
             'autosubscription_max_count': 4
             }

    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)

    items['signature'] = signature
    print('&'.join(['{}={}'.format(k, v) for k, v in items.items()]))

    return render(request, 'pay.html',
                  context={'items': items, 'action': '{}/pay'.format(params.get('address'))})


def recurrent_payment(request, world):
    params = settings.WORLD.get(world)
    order_id = str(random.randint(0, 999999))
    amount = random.randint(0, 100)

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             "signature": '5e521a2ba0abc63589577c3084774907d9d09610',
             "merchant": params.get('merchant'),
             "testing": '0',
             "amount": 1234,
             "description": 'Заказ №{}'.format(order_id),
             "order_id": order_id,
             "client_phone": '+7 (951) 592-68-36',
             "client_id": 1,
             "client_email": 'ye.miloserdova@modulbank.ru',
             "sysinfo": '',
             # "callback_url": 'http://f95f65dc.ngrok.io/callback',
             'custom_order_id': 'erfrgerg',
             "receipt_contact": 'ye.miloserdova@modulbank.ru',
             "receipt_items": json.dumps(ITEMS, sort_keys=True),
             "first_recurrent_transaction": "PojsXyF9c66mn4FNGzgvyx"
             }

    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)

    items['signature'] = signature
    print('&'.join(['{}={}'.format(k, v) for k, v in items.items()]))
    r = requests.post('{}/api/v1/next_recurrent'.format(params.get('address')), data=json.dumps(items), verify=False)
    return HttpResponse(r.content)


# Создание счета
def bill_post_api(request, world):
    params = settings.WORLD.get(world)

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             "merchant": params.get('merchant'),
             "testing": '0',
             "amount": 1234,
             "description": 'Оплата консультаций.',
             "client_email": 'da.smirnov@modulbank.ru',
             "send_letter": '0',
             "receipt_contact": 'da.smirnov@modulbank.ru',
             "receipt_items": json.dumps(ITEMS, sort_keys=True),
             'lifetime': 86400,
             "reusable": "1"
             }
    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature

    print(json.dumps(items))
    # print(items)
    r = requests.post('{}/api/v1/bill/'.format(params.get('address')), data=json.dumps(items), verify=False)  # data
    # print(r.content)
    return HttpResponse(r.content)


# Изменение данных счета
def bill_put_api(request, world):
    params = settings.WORLD.get(world)

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             "merchant": params.get('merchant'),
             "id": "uZPQ7Dm036kiDNrRElgEPj",
             "active": "0"
             }
    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature

    print(json.dumps(items))

    r = requests.put('{}/api/v1/bill/'.format(params.get('address')), data=items, verify=False)  # data
    return HttpResponse(r.content)


def bill_get_api(request, world):
    params = settings.WORLD.get(world)

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "merchant": params.get('merchant'),
             "id": "q8r3rYFSTOr0r0cpHnYG5E",
             }
    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature

    print(json.dumps(items))

    r = requests.get('{}/api/v1/bill/'.format(params.get('address')), params=items, verify=False)  # data
    return HttpResponse(r.content)


def transactions_get_info(request, world):
    params = settings.WORLD.get(world)

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "merchant": params.get('merchant'),
             "timestamp_from": "2019-05-17T00:00:00.123+04:00",
             "timestamp_to": "2019-05-25T00:00:00.123+04:00",
             "bill": "q8r3rYFSTOr0r0cpHnYG5E"
             }
    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature

    print(json.dumps(items))

    r = requests.get('{}/api/v1/transactions/'.format(params.get('address')), params=items, verify=False)  # data
    return HttpResponse(r.content)


def transaction_get_info(request, world):
    params = settings.WORLD.get(world)

    secret_key = params.get('secret_key')
    merchant = params.get('merchant')
    tx = 'Fvaz8C53i13CdKj1rFSxeZ'

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "transaction_id": tx,
             "merchant": merchant,
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             }
    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature

    r = requests.get('{}/api/v1/transaction/'.format(params.get('address')), params=items, verify=False)
    return HttpResponse(r.content)


def refund_api(request, world):
    params = settings.WORLD.get(world)
    secret_key = params.get('secret_key')

    items = {
        'merchant': params.get('merchant'),
        'amount': '10',
        'transaction': 'Fvaz8C53i13CdKj1rFSxeZ',
        'unix_timestamp': get_unix_timestamp(datetime.now()),
    }
    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature
    r = requests.post('{}/api/v1/refund/'.format(params.get('address')), data=items, verify=False)

    resp = r.json()
    return HttpResponse(r.content)


def callback(request):
    logger.info('Got callback: {}'.format(dict(request.POST)))
    return HttpResponse()


def success_response(request):
    data = request.POST.dict() or json.loads(request.body.decode())
    print(json.dumps(data, indent=4))
    return HttpResponse()


def finish_3ds(request, world):
    params = settings.WORLD.get(world)

    data = {'MD': request.POST['MD'],
            'PaRes': request.POST['PaRes']}
    finish_r = requests.post('{}/api/v1/finish-3ds'.format(params.get('address')), data=data, verify=False)
    return JsonResponse(finish_r.json())


def capture(request, world):
    params = settings.WORLD.get(world)
    items = {"transaction": "bFb44PguiVcq1rNTTxDNt3",
             "amount": 10,
             "testing": 1,
             "merchant": params.get('merchant'),
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             "unix_timestamp": get_unix_timestamp(datetime.now())
             }

    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    items['signature'] = signature

    r = requests.post('{}/api/v1/capture/'.format(params.get('address')), data=items, verify=False)
    return HttpResponse(r.content)


def extended_manual_bill(request, world):
    params = settings.WORLD.get(world)

    PAGE_CONTENT = {
        "form_title": "Тетрадь",
        "form_content": [
            {
                "name": "Для работ по",
                "value": "русскому языку"
            },
            {
                "name": "учени",
                "value": "цы 10 класса а"
            },
            {
                "name": "Пупыркиной",
                "value": "Ксении"
            },
        ]
    }

    items = {"unix_timestamp": get_unix_timestamp(datetime.now()),
             "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
             "merchant": params.get('merchant'),
             "testing": '0',
             "amount": 10,
             "description": 'Оплата консультаций.',
             "client_email": 'ye.miloserdova@modulbank.ru',
             "receipt_contact": 'ye.miloserdova@modulbank.ru',
             "receipt_items": json.dumps(ITEMS, sort_keys=True),
             "signature": '5e521a2ba0abc63589577c3084774907d9d09610',
             "payment_page_content": json.dumps(PAGE_CONTENT, sort_keys=True)
             }

    secret_key = params.get('test_secret_key') if str(items.get('testing')) == '1' else params.get('secret_key')
    signature = get_signature(secret_key, items)
    print(get_unix_timestamp(datetime.now()))

    items['signature'] = signature

    print(json.dumps(items))

    r = requests.post('{}/api/v1/extended_bill/'.format(params.get('address')), data=items, verify=False)
    return HttpResponse(r.content)


def cancel_subscription(request, world):
    params = settings.WORLD.get(world)

    items = {
        "unix_timestamp": get_unix_timestamp(datetime.now()),
        "first_recurrent_transaction": 'bxuLNewXaSupl1iFGADkP6',
        "merchant": params.get('merchant'),
        "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
    }
    secret_key = params.get('secret_key')
    signature = get_signature(secret_key, items)
    print(get_unix_timestamp(datetime.now()))

    items['signature'] = signature

    print(json.dumps(items))

    r = requests.post('{}/api/v1/recurrent_subscription/cancel'.format(params.get('address')), data=items, verify=False)
    return HttpResponse(r.content)


def change_subscription(request, world):
    params = settings.WORLD.get(world)

    items = {
        "unix_timestamp": get_unix_timestamp(datetime.now()),
        "first_recurrent_transaction": 'vgMsmksAfOOX69VM4wIIeZ',
        "merchant": params.get('merchant'),
        "salt": 'GfudKOAsXobWVpNovJHCreKmJXNkLqtf',
        "interval": 'month',
        "period": '3',
        "max_count": '6',
    }
    secret_key = params.get('secret_key')
    signature = get_signature(secret_key, items)
    print(get_unix_timestamp(datetime.now()))

    items['signature'] = signature

    print(json.dumps(items))

    r = requests.post('{}/api/v1/recurrent_subscription/change'.format(params.get('address')), data=items, verify=False)
    return HttpResponse(r.content)